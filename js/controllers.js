(function(){

    'use strict';

    var controllers = angular.module('reddit.controllers', []);

    controllers.controller('AppCtrl', function($scope) {
        $scope.name = 'Module';
    });

    controllers.controller('HomeListCtrl', function($scope, redditData) {
        redditData.showHomePage().then(function(data) {
            $scope.list = data.data;
        });
    });

    controllers.controller('ShotsListCtrl', function($scope, redditData, $routeParams) {
        var param = $routeParams.param;

        redditData.showPage(param).then(function(data) {
            $scope.list = data.data;
        });
    });

})();