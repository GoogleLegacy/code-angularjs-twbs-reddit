(function() {

    'use strict';

    var directives = angular.module('reddit.directives', []);

    directives.directive('navigation', function() {
        return {
            restrict: 'E',
            templateUrl: 'templates/navigation.html',
            controller: function() {
                this.tab = 1;

                this.selectTab = function(setTab) {
                    this.tab = setTab;
                };

                this.isSelected = function(checkTab) {
                    return this.tab === checkTab;
                };
            },
            controllerAs: 'tab'
        }
    });


    directives.directive('fallbackImage', function () {
        var fallbackImageUrl = 'img/no_image.png';

        var setFallbackImage = function(element) {
            element.attr('src', fallbackImageUrl);
        };

        return {
            restrict: 'A',
            link: function($scope, element, attr) {
                $scope.$watch(function() {
                    return attr.ngSrc;
                }, function () {
                    if (!attr.ngSrc) {
                        setFallbackImage(element);
                    }
                });

                element.bind('error', function() {
                    setFallbackImage(element);
                });
            }
        };
    });

})();