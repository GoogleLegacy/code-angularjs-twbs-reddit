(function() {

    'use strict';

    var app = angular.module('reddit',
        [
            'ngRoute',
            'reddit.controllers',
            'reddit.filters',
            'reddit.services',
            'reddit.directives'
        ]
    );

    app.config(function($routeProvider) {
        $routeProvider
            .when("/:param", {
                templateUrl: "templates/posts.html",
                controller: "ShotsListCtrl"
            })
            .otherwise({
                templateUrl: "templates/posts.html",
                controller: "HomeListCtrl"
            });
    });

})();