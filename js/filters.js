(function(){

    'use strict';

    var filters = angular.module('reddit.filters', []);

    filters.filter('redditDate', function($filter) {

        return function(dateValue, format) {

            if (dateValue) {
                var date = new Date(dateValue*1000);
            }

            return $filter('date')(date, format);
        }
    });

})();