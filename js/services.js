(function(){

    'use strict';

    var services = angular.module('reddit.services', []);

    services.factory('redditData', function($http) {
        var path = 'https://www.reddit.com/';

        return {
            showPage: function(type) {
                return $http.get(path + type + '.json');
            },
            showHomePage: function() {
                return $http.get(path + '.json');
            }
        }
    });

})();